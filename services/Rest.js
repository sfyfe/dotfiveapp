import React from 'react'
import {
  AsyncStorage
} from 'react-native'

function Call (type, url, data, cb) {
  AsyncStorage.getItem('token', (_, token) => {
    if (!/^\//.test(url)) {
      url = '/' + url
    }
    let fetchOptions = {
      method: type,
      headers: {}
    }
    if (data && ['POST', 'PUT', 'PATCH'].indexOf(type) > -1) {
      fetchOptions.headers['Accept'] = 'application/json'
      fetchOptions.headers['Content-Type'] = 'application/json'
      fetchOptions.body = JSON.stringify(data)
    }
    if (token) {
      fetchOptions.headers['Authorization'] = 'Bearer ' + token
    }
    try {
      fetch(
        'http://localhost:1337' + url,
        fetchOptions
      ).then(response => {
        response.json().then(data => {
          if (!response.ok) {
            cb(data.message, response.status)
          } else {
            cb(null, response.status, data)
          }
        })
        return response
      }).catch(error => {
        cb(error)
      })
    } catch (e) {
      cb(e.message)
    }
  })
}

const Rest = {
  get: (url, cb) => {
    return Call('GET', url, false, cb)
  },
  post: (url, data, cb) => {
    return Call('POST', url, data, cb)
  },
  put: (url, data, cb) => {
    return Call('PUT', url, data, cb)
  },
  patch: (url, data, cb) => {
    return Call('PATCH', url, data, cb)
  },
  delete: (url, cb) => {
    return Call('DELETE', url, false, cb)
  }
}

export default Rest
