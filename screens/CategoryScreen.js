import React, { Component } from 'react'
import {
  Alert,
  AsyncStorage,
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'

import Rest from '../services/Rest'

const {width} = Dimensions.get('window')

export default class CategoryScreen extends Component {
  constructor (props) {
    super(props)
    this.state = { userID: false, category: false, itemName: '' }
  }

  componentDidMount () {
    AsyncStorage.getItem('userID', (_, userID) => {
      const category = this.props.navigation.state.params
      this.setState({category, userID})
      this.interval = setInterval(() => this.updateCategory(), 5000)
    })
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  updateCategory () {
    const id = this.state.category.id
    Rest.get(`category/${id}`, (err, status, category) => {
      if (err) {
        Alert.alert('Error', err)
      } else {
        category.item.forEach((item1) => {
          let found = false
          this.state.category.item.forEach((item2) => {
            if (item1.id === item2.id) {
              found = true
            }
          })
          item1.new = !found
        })
        this.setState({category})
      }
    })
  }

  addItem () {
    if (this.state.itemName === '') {
      Alert.alert('Error', 'Please enter an item name')
    } else {
      const data = {
        name: this.state.itemName,
        category: this.state.category.id,
        createdBy: this.state.userID
      }
      Rest.post('item', data, (err, status, item) => {
        if (err) {
          Alert.alert('Error', err)
        } else {
          this.updateCategory()
        }
      })
    }
  }

  render () {
    return (
      <View style={styles.itemWrap}>
        <FlatList
          data={this.state.category.item}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item}) => {
            return <View
              style={[styles.item, item.new ? styles.newItem : false]}
            >
              <Text style={styles.itemText}>{item.name}</Text>
            </View>
          }}
        />
        <View style={styles.addItemWrap}>
          <TextInput
            style={styles.addItemInput}
            onChangeText={(itemName) => this.setState({itemName})}
            value={this.state.itemName}
            underlineColorAndroid={'transparent'}
            placeholder={'New Item'}
          />
          <TouchableOpacity
            style={styles.addItemButton}
            onPress={() => this.addItem()}
          >
            <Text style={styles.addItemButtonText}>Add</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const fontSize = 18

const styles = StyleSheet.create({
  itemWrap: {
    flex: 1,
    backgroundColor: '#fff'
  },
  item: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)'
  },
  itemText: {
    textAlign: 'center',
    fontSize,
    padding: fontSize / 2
  },
  newItem: {
    backgroundColor: 'rgba(0,255,0,0.2)'
  },
  addItemWrap: {
    width,
    flexDirection: 'row'
  },
  addItemInput: {
    width: width - (fontSize * 4.5),
    textAlign: 'left',
    fontSize,
    padding: fontSize / 2,
    margin: fontSize / 2,
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)'
  },
  addItemButton: {
    backgroundColor: '#4cd964',
    marginTop: fontSize / 2,
    marginBottom: fontSize / 2
  },
  addItemButtonText: {
    textAlign: 'center',
    fontSize,
    padding: fontSize / 2,
    color: '#fff',
    fontWeight: 'bold'
  }
})
