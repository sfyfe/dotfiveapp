import React, { Component } from 'react'
import {
  Alert,
  AsyncStorage,
  Dimensions,
  FlatList,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'

import Icon from 'react-native-vector-icons/dist/FontAwesome'

import Rest from '../services/Rest'

const {height, width} = Dimensions.get('window')

export default class CategoriesScreen extends Component {
  constructor (props) {
    super(props)
    this.state = { email: '', password: '', userID: false, categories: [] }
  }

  componentDidMount () {
    AsyncStorage.getItem('userID', (_, userID) => {
      this.interval = setInterval(() => this.updateCategories(), 5000)
      if (userID !== null) {
        this.setState({userID})
      }
    })
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  updateCategories (first) {
    // only get categories if user is logged in
    if (this.state.userID) {
      Rest.get(`category`, (err, status, categories) => {
        if (err) {
          Alert.alert('Error', err)
        } else {
          categories.forEach((category1) => {
            let found = false
            if (first) {
              found = true
            } else {
              this.state.categories.forEach((category2) => {
                if (category1.id === category2.id) {
                  found = true
                }
              })
            }
            category1.new = !found
          })
          this.setState({categories})
        }
      })
    }
  }

  login () {
    const data = {
      email: this.state.email,
      password: this.state.password
    }
    Rest.post('login', data, (err, status, response) => {
      if (err) {
        Alert.alert('Error', err)
      } else {
        AsyncStorage.multiSet([
          ['userID', `${response.userID}`], ['token', response.token]
        ], _ => {
          this.setState({userID: response.userID}, () => {
            this.updateCategories(true)
          })
        })
      }
    })
  }

  register () {
    const data = {
      email: this.state.email,
      password: this.state.password
    }
    Rest.post('user', data, (err, status, user) => {
      if (err) {
        Alert.alert('Error', err)
      } else {
        this.login()
      }
    })
  }

  logout () {
    AsyncStorage.multiRemove(['userID', 'token'], () => {
      this.setState({userID: false, email: '', password: ''})
    })
  }

  addCategory () {
    if (this.state.categoryName === '') {
      Alert.alert('Error', 'Please enter a category name')
    } else {
      const data = {
        name: this.state.categoryName,
        createdBy: this.state.userID
      }
      Rest.post('category', data, (err, status, category) => {
        if (err) {
          Alert.alert('Error', err)
        } else {
          this.updateCategories()
        }
      })
    }
  }

  viewCategory (category) {
    this.props.navigation.navigate('Category', { ...category })
  }

  render () {
    return (
      this.state.userID ? (
        <View style={styles.categoriesWrap}>
          <FlatList
            data={this.state.categories}
            keyExtractor={(category, index) => `${category.id}`}
            renderItem={({item}) => {
              return <TouchableOpacity
                style={[styles.category, item.new ? styles.newCategory : false]}
                onPress={() => this.viewCategory(item)}
              >
                <Text style={styles.categoryName}>{item.name}</Text>
                <Icon
                  name='chevron-right'
                  size={fontSize}
                  style={styles.categoryIcon}
                />
              </TouchableOpacity>
            }}
          />
          <View style={styles.addCategoryWrap}>
            <TextInput
              style={styles.addCategoryInput}
              onChangeText={(categoryName) => this.setState({categoryName})}
              value={this.state.categoryName}
              underlineColorAndroid={'transparent'}
              placeholder={'New Category'}
            />
            <TouchableOpacity
              style={styles.addCategoryButton}
              onPress={() => this.addCategory()}
            >
              <Text style={styles.addCategoryButtonText}>Add</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => this.logout()}
            style={styles.logoutButton}
          >
            <Text style={styles.logoutButtonText}>Logout</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <KeyboardAvoidingView
          style={styles.login}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          enabled
        >
          <TextInput
            style={styles.loginInput}
            onChangeText={(email) => this.setState({email})}
            value={this.state.email}
            autoFocus
            autoCapitalize={'none'}
            autoCorrect={false}
            keyboardType={'email-address'}
            underlineColorAndroid={'transparent'}
            placeholder={'E-mail'}
          />
          <TextInput
            style={styles.loginInput}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            autoCapitalize={'none'}
            autoCorrect={false}
            secureTextEntry
            underlineColorAndroid={'transparent'}
            placeholder={'Password'}
          />
          <TouchableOpacity
            onPress={() => this.login()}
            style={styles.loginButton}
          >
            <Text style={styles.loginButtonText}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.register()}
            style={styles.loginButton}
          >
            <Text style={styles.loginButtonText}>Register</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      )
    )
  }
}

const fontSize = 18

const styles = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  loginInput: {
    width: width - (fontSize * 2),
    textAlign: 'center',
    fontSize,
    padding: fontSize / 2,
    marginBottom: fontSize / 2,
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)'
  },
  loginButton: {
    width: width - (fontSize * 2),
    backgroundColor: '#007aff',
    marginBottom: fontSize / 2
  },
  loginButtonText: {
    textAlign: 'center',
    fontSize,
    padding: fontSize / 2,
    color: '#fff',
    fontWeight: 'bold'
  },
  categoriesWrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  categories: {
    flex: 1,
    height: height - (fontSize * 6),
    backgroundColor: '#ff0000'
  },
  category: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)'
  },
  newCategory: {
    backgroundColor: 'rgba(0,255,0,0.2)'
  },
  categoryName: {
    fontSize,
    padding: fontSize / 2,
    width: width - (fontSize * 2)
  },
  categoryIcon: {
    marginTop: fontSize / 2
  },
  addCategoryWrap: {
    width,
    flexDirection: 'row'
  },
  addCategoryInput: {
    width: width - (fontSize * 4.5),
    textAlign: 'left',
    fontSize,
    padding: fontSize / 2,
    margin: fontSize / 2,
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)'
  },
  addCategoryButton: {
    backgroundColor: '#4cd964',
    marginTop: fontSize / 2,
    marginBottom: fontSize / 2
  },
  addCategoryButtonText: {
    textAlign: 'center',
    fontSize,
    padding: fontSize / 2,
    color: '#fff',
    fontWeight: 'bold'
  },
  logoutButton: {
    width: width - (fontSize * 1),
    backgroundColor: '#007aff',
    marginBottom: fontSize / 2
  },
  logoutButtonText: {
    textAlign: 'center',
    fontSize,
    padding: fontSize / 2,
    color: '#fff',
    fontWeight: 'bold'
  }
})
