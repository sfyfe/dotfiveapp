import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  YellowBox
} from 'react-native'

import Icon from 'react-native-vector-icons/dist/FontAwesome'

import { createStackNavigator } from 'react-navigation'

import CategoriesScreen from './screens/CategoriesScreen'
import CategoryScreen from './screens/CategoryScreen'

// suppress react-native's own bugs, which are causing warnings
YellowBox.ignoreWarnings([
  'Warning: isMounted(...) is deprecated',
  'Class RCTCxxModule',
  'Module RCTImageLoader'
])

const fontSize = 18

const styles = StyleSheet.create({
  headerTitle: {
    fontSize: fontSize,
    padding: fontSize / 2,
    textAlign: 'center',
    flex: 1
  },
  backButtonWrap: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: fontSize / 2
  },
  backButtonIcon: {
    marginLeft: fontSize / 2,
    marginTop: fontSize / 4
  }
})

const BarsNavigator = createStackNavigator({
  Categories: {
    screen: CategoriesScreen
  },
  Category: {
    screen: CategoryScreen,
    navigationOptions: navigation => {
      if (navigation.hasOwnProperty('navigation')) {
        // Necessary for Android
        navigation = navigation.navigation
      }
      const name = navigation.state.params.name
      const resultObj = {
        headerStyle: {
          backgroundColor: '#fff',
          height: fontSize * 2.3
        },
        headerTitle: () => {
          return <Text style={styles.headerTitle}>{name}</Text>
        },
        headerLeft: () => {
          return <TouchableOpacity
            style={styles.backButtonWrap}
            onPress={() => navigation.goBack(null)}
          >
            <Icon name='angle-left'
              size={fontSize * 1.5}
              style={styles.backButtonIcon}
            />
          </TouchableOpacity>
        }
      }
      return resultObj
    }
  }
})

export default class App extends Component {
  render () {
    return (
      <BarsNavigator
        screenProps={this.state}
      />
    )
  }
}
