# Setup

These instructions assume you are using macOS, in order to use XCode's simulator.

## React Native Dependencies

Install [Node](https://nodejs.org)  

Install [Homebrew](https://brew.sh)  

```
brew install watchman
```

## iOS dependencies

Install [Xcode](https://itunes.apple.com/gb/app/xcode/id497799835)

Open it, in order to trigger the installation of all required dependencies

Make sure the `Command Line Tools` path is set in XCode > Preferences > Locations

## Android Dependencies

Install [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

Install version 8 of the [Java SE JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) (as of writing this, 9 is the latest version, which does not work with React Native's build process).

Follow React Native's [Android Development Environment instructions](https://facebook.github.io/react-native/docs/getting-started.html#android-development-environment)

Install the personal version of [Genymotion](https://www.genymotion.com/download/)

Set up an appropriate virtual device in Genymotion. To access their list of devices, you will need to create a Genymotion account and log in.

Run the Android virtual device.

## Run in a Virtual Device

`cd path/to/project`

### iOS
`react-native run-ios --simulator="iPhone X"`

### Android
`react-native run-android`
